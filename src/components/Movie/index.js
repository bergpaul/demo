import React from "react";
import "./Movie.css";
import { connect } from "react-redux";
import { loadMovie } from "../../actions/movies";

class Movie extends React.Component {
  state = {
    movie: {}
  };

  async componentDidUpdate() {
    if (this.props.movieId !== -1 && !this.props.movie) {
      this.props.loadMovie(this.props.movieId);
    }
  }

  render() {
    const formatDate = date => {
      if (!date) return "Date inconnue";
      const d = new Date(date);
      const format = str => ("0" + str).slice(0, 2);
      return `${format(d.getDay() + 1)}/${format(
        d.getMonth() + 1
      )}/${d.getFullYear()}`;
    };

    const { movie, movieId, closeMovie, openMovie } = this.props;

    if (movieId === -1) return <></>;

    return (
      <div onClick={closeMovie} className="background">
        <div onClick={e => e.stopPropagation()} className="card">
          {movie ? (
            <>
              {movie.poster_path ? (
                <div className="poster">
                  <img
                    height="450"
                    width="300"
                    src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${
                      movie.poster_path
                    }`}
                    alt=""
                  />
                </div>
              ) : (
                ""
              )}
              <div className="text">
                <p className="title">{movie.title}</p>
                {movie.genres.length > 0 ? (
                  <p className="cast">
                    {movie.genres
                      .slice(0, movie.genres.length - 2)
                      .map(({ name }, id) => (
                        <span key={id}>{name} • </span>
                      ))}
                    {movie.genres[movie.genres.length - 1].name}
                  </p>
                ) : (
                  ""
                )}
                <p className="cast">
                  {formatDate(movie.release_date)} • {movie.vote_average}
                  /10
                </p>
                <p className="overview">{movie.overview}</p>
                {movie.credits.cast.length > 0 ? <p className="cast">
                  Avec{" "}
                  {movie.credits.cast
                    .slice(0, movie.credits.cast.length - 2)
                    .map((actor, id) => (
                      <span key={id}>{actor.name}, </span>
                    ))}
                  {movie.credits.cast[movie.credits.cast.length - 1].name}
                </p>: ''}
                <p className="similar">
                  {movie.similar.results.length > 0
                    ? movie.similar.results.map((movie, id) => (
                        <span
                          onClick={() => openMovie(movie.id)}
                          key={id}
                          className="button"
                        >
                          {movie.title}
                        </span>
                      ))
                    : "Aucun film similaire"}
                </p>
                <p>
                  <span onClick={closeMovie} className="button">Fermer</span>
                </p>
              </div>
            </>
          ) : (
            "Chargement..."
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ movies }, props) => ({
  movie: movies.find(movie => movie.id === props.movieId)
});

const mapDispatchToProps = dispatch => ({
  loadMovie: movieId => loadMovie(movieId, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Movie);
