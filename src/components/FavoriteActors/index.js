import React from "react";
import "./FavoriteActors.css";
import { connect } from "react-redux";

class FavoriteActors extends React.Component {
  render() {
    return (
      <div className="FavoriteActors">
        <p className="header">Acteurs favoris</p>
        <ul>
          {this.props.actors.map(actor => (
            <li
              className={
                this.props.selectedActors.includes(actor.id) ? "selected" : ""
              }
              onClick={() => this.props.onActorClicked(actor.id)}
              key={actor.id}
            >
              <div className={`avatar`}>
                <img
                  width="40"
                  alt={`Profile for ${actor.name}`}
                  src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${
                    actor.images.profiles[0].file_path
                  }`}
                />
              </div>
              <p>{actor.name}</p>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = ({ favoriteActors, actors }) => ({
  actors: actors.filter(actor => favoriteActors.includes(actor.id))
});

export default connect(mapStateToProps)(FavoriteActors);
