import React from "react";

export default class Loading extends React.Component {
  state = {
    loading: true
  };

  render() {
    return this.state.loading ? <div>Loading</div> : this.props.children;
  }
}
