import React from "react";
import { connect } from "react-redux";
import "./MovieList.css";

import { flatten, unique } from "../../utils/array";

class MovieList extends React.Component {
  state = {
    pageIndex: 0,
    sortAsc: false
  };

  render() {
    const formatDate = date => {
      if (!date) return "Date inconnue";
      const d = new Date(date);
      const format = str => ("0" + str).slice(0, 2);
      return `${format(d.getDay() + 1)}/${format(
        d.getMonth() + 1
      )}/${d.getFullYear()}`;
    };

    const movies = unique(
      flatten(this.props.actors.map(actor => actor.movie_credits.cast))
    )
      .filter(movie => movie.title.toLowerCase().includes(""))
      .sort(
        (a, b) =>
          this.state.sortAsc
            ? a.vote_average > b.vote_average
            : a.vote_average < b.vote_average
      );
    const { pageIndex } = this.state;
    return (
      <div className="MovieList">
        <div
          onClick={() => this.setState({ sortAsc: !this.state.sortAsc })}
          className="filter"
        >
          {this.state.sortAsc ? "▲ NOTE ▲" : "▼ NOTE ▼"}
        </div>
        {movies.slice(pageIndex * 10, (pageIndex + 1) * 10).map((movie, id) => (
          <div
            key={id}
            onClick={() => this.props.openMovie(movie.id)}
            className="movie"
          >
            {movie.poster_path ? (
              <img
                src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${
                  movie.poster_path
                }`}
                alt=""
              />
            ) : (
              <div className="placeholder" />
            )}
            <div>
              <p className="title">{movie.title}</p>
              <p className="overview">
                {formatDate(movie.release_date)} • {movie.vote_average}
                /10
              </p>
              <p className="overview">
                {movie.overview.slice(0, 200)}
                {movie.overview.length > 200 ? "..." : ""}
              </p>
            </div>
          </div>
        ))}
        <div>
          <span
            onClick={() =>
              this.state.pageIndex !== 0
                ? this.setState({ pageIndex: pageIndex - 1 })
                : ""
            }
            className="button"
          >
            {"<"}
          </span>
          {this.state.pageIndex}
          <span
            onClick={() =>
              this.state.pageIndex !== Math.floor(movies.length / 10)
                ? this.setState({ pageIndex: pageIndex + 1 })
                : ""
            }
            className="button"
          >
            >
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ actors }, { selectedActors }) => ({
  actors: actors.filter(actor => selectedActors.includes(actor.id))
});

export default connect(mapStateToProps)(MovieList);
