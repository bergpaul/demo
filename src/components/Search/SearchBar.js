import React from "react";
import "./SearchBar.css";

import { connect } from "react-redux";
import { flatten, unique } from "../../utils/array";
import { search } from '../../utils/api';

class SearchBar extends React.Component {
  state = {
    query: "",
    selected: 0,
    results: [],
  };

  getCandidates = () => {
    const { query, results } = this.state;
    const { movies, actors } = this.props;
    const candidates = unique([
      ...results,
      ...movies,
      ...flatten(actors.map(actor => actor.movie_credits.cast))
    ]).filter(
      movie => query !== "" && movie.title.toLowerCase().includes(query.toLowerCase())
    );

    return candidates;
  };

  onKeyDown = e => {
    switch (e.keyCode) {
      case 40:
        this.setState({
          selected: Math.min(
            this.getCandidates().length - 1,
            this.state.selected + 1
          )
        });
        break;
      case 38:
        this.setState({ selected: Math.max(0, this.state.selected - 1) });
        break;
      case 13:
        this.setState({ query: "", selected: 0 });
        this.props.openMovie(this.getCandidates()[this.state.selected].id);
        break;
      default:
        break;
    }
  };

  handleChange = e => {
    this.setState({ query: e.target.value });
    if(e.target.value !== '') search(e.target.value)
      .then(({results}) => this.setState({results}));
  }

  render() {
    const { query, selected } = this.state;

    return (
      <div onKeyDown={this.onKeyDown} style={{ width: "100%" }}>
        <input
          onChange={this.handleChange}
          value={query}
          placeholder="Recherche"
          className="SearchBar"
          type="text"
        />
        <div className="results">
          {this.getCandidates().map((candidate, id) => {
            const index = candidate.title.toLowerCase().indexOf(query);
            return (
              <div
                onClick={() => {
                  this.props.openMovie(candidate.id);
                  this.setState({ query: "", selected: 0 });
                }}
                className={`result ${id === selected ? "selected" : ""}`}
              >
                {candidate.title.slice(0, index)}
                {<b>{candidate.title.slice(index, index + query.length)}</b>}
                {candidate.title.slice(index + query.length)}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ movies, actors }) => ({
  movies,
  actors
});

export default connect(mapStateToProps)(SearchBar);
