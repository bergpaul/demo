import { combineReducers } from "redux";
import actors from "./actors";
import favoriteActors from "./favoriteActors";
import movies from "./movies";

export default combineReducers({
  actors,
  favoriteActors,
  movies
});
