import { SET_MOVIE } from "../actions/movies";

export default (state = [], action) => {
  switch (action.type) {
    case SET_MOVIE:
      return [...state, action.movie];
    default:
      return state;
  }
};
