import { SET_ACTOR, SET_ACTORS } from "../actions/actors";

export default (state = [], action) => {
  switch (action.type) {
    case SET_ACTOR:
      return [...state, action.actor];
    case SET_ACTORS:
      return [...state, ...action.actors];
    default:
      return state;
  }
};
