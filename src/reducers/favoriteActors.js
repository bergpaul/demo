import { SET_FAVORITE_ACTOR } from "../actions/favoriteActors";
import { actors } from "../config.json";

export default (state = actors, action) => {
  switch (action.type) {
    case SET_FAVORITE_ACTOR:
      return [...state, action.actorId];
    default:
      return state;
  }
};
