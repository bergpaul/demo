import React from "react";
import "./App.css";

import SearchBar from "./components/Search/SearchBar";
import FavoriteActors from "./components/FavoriteActors";
import MovieList from "./components/MovieList";
import Movie from "./components/Movie";

import { actors } from "./config.json";
import { connect } from "react-redux";
import { loadActors } from "./actions/actors";

class App extends React.Component {
  state = {
    selectedActors: actors,
    selectedMovie: -1
  };

  componentDidMount() {
    this.props.loadActors(actors);
  }

  openMovie = selectedMovie => this.setState({ selectedMovie });
  closeMovie = () => this.setState({ selectedMovie: -1 });

  render() {
    return (
      <div className="App">
        <Movie
          openMovie={this.openMovie}
          closeMovie={this.closeMovie}
          movieId={this.state.selectedMovie}
        />
        <FavoriteActors
          selectedActors={this.state.selectedActors}
          onActorClicked={id =>
            this.state.selectedActors.includes(id)
              ? this.setState({
                  selectedActors: this.state.selectedActors.filter(
                    a => a !== id
                  )
                })
              : this.setState({
                  selectedActors: [...this.state.selectedActors, id]
                })
          }
        />
        <div>
          <SearchBar openMovie={this.openMovie} />
          <MovieList
            openMovie={this.openMovie}
            selectedActors={this.state.selectedActors}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  loadActors: actorIds => loadActors(actorIds, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
