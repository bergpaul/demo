export const flatten = (arr, result = []) => {
  for (let i = 0, length = arr.length; i < length; i++) {
    const value = arr[i];
    if (Array.isArray(value)) {
      flatten(value, result);
    } else {
      result.push(value);
    }
  }
  return result;
};

export const unique = arr => {
  const map = {};
  return arr.filter(el => {
    if (map[el.title]) return false;
    map[el.title] = true;
    return true;
  });
};
