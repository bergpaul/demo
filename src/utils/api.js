import { APIKey } from "../config.json";

async function get(endpoint, params = {}, authentified = true) {
  const url = new URL(`https://api.themoviedb.org/3/${endpoint}`);
  if (authentified) url.searchParams.append("api_key", APIKey);
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  const res = await fetch(url);
  return await res.json();
}

export async function getActor(id) {
  return await get(`person/${id}`, {
    append_to_response: "images,movie_credits"
  });
}

export async function getMovie(id) {
  return await get(`movie/${id}`, { append_to_response: "similar,credits" });
}

export async function search(query) {
  return await get(`search/movie`, { query });
}
