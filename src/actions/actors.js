import { getActor } from "../utils/api";

export const SET_ACTOR = "SET_ACTOR";
export const setActor = actor => ({
  type: SET_ACTOR,
  actor
});

export const SET_ACTORS = "SET_ACTORS";
export const setActors = actors => ({
  type: SET_ACTORS,
  actors
});

export async function loadActor(actorId, dispatch) {
  const actor = await getActor(actorId);
  dispatch(setActor(actor));
}

export async function loadActors(actorIds, dispatch) {
  const actors = await Promise.all(actorIds.map(actorId => getActor(actorId)));
  dispatch(setActors(actors));
}
