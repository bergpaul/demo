export const SET_FAVORITE_ACTOR = "SET_FAVORITE_ACTOR";
export const setFavoriteActor = actorId => ({
  type: SET_FAVORITE_ACTOR,
  actorId
});
