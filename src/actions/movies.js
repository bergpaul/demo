import { getMovie } from "../utils/api";

export const SET_MOVIE = "SET_MOVIE";
export const setMovie = movie => ({
  type: SET_MOVIE,
  movie
});

export async function loadMovie(movieId, dispatch) {
  const movie = await getMovie(movieId);
  dispatch(setMovie(movie));
}
